# Domain Theme switch

Domain Theme Switch module for Drupal 10+ community users.

* This module provides the administrator interface to manage the theme.
* The interface provides very basic setting to manage the theme
  against each domain.

## Configuration:

Domain Theme module have configuration setting page
to set theme against each domain.

Configuration page path:
`/admin/config/domain/domain_theme_switch/config`

## Dependencies:

- domain
